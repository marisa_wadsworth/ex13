﻿using System;

namespace ex13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Hello My Name Is Marisa");
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
